SET GLOBAL local_infile=1;

use data_has_power;


CREATE TABLE IF NOT EXISTS tools (
    id VARCHAR(50) NOT NULL,
    tool_type VARCHAR(50) NOT NULL,
    prod_line_no INT NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


LOAD DATA LOCAL INFILE '/tmp/source/tools.csv'
INTO TABLE tools
FIELDS TERMINATED BY ','
    ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(id, tool_type, prod_line_no);
