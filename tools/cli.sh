# Start cluster
docker-compose up -d

# Stop cluster
docker-compose down


# Go to usr/kafka_2.13-3.0.0/bin

# Create topic
docker exec broker \
kafka-topics --bootstrap-server broker:9093 \
             --create \
             --topic quickstart-events \
             --partitions 1 \
             --replication-factor 1

# List details of topic
./kafka-topics.sh --describe --topic quickstart-events --bootstrap-server localhost:9093

# Write to topic
docker exec --interactive --tty broker \
kafka-console-producer --bootstrap-server broker:9093 \
                       --topic quickstart-events

# Read from topic
docker exec --interactive --tty broker \
kafka-console-consumer --bootstrap-server broker:9093 \
                        --topic quickstart-events \
                        --from-beginning



# KSQL

# Login
docker exec -it ksqldb-cli ksql http://ksqldb-server:8088

# Create a stream
CREATE STREAM riderLocations (profileId VARCHAR, latitude DOUBLE, longitude DOUBLE)
  WITH (kafka_topic='locations', value_format='json', partitions=1);

# Create materialized views
CREATE TABLE currentLocation AS
  SELECT profileId,
         LATEST_BY_OFFSET(latitude) AS la,
         LATEST_BY_OFFSET(longitude) AS lo
  FROM riderlocations
  GROUP BY profileId
  EMIT CHANGES;

CREATE TABLE ridersNearMountainView AS
  SELECT ROUND(GEO_DISTANCE(la, lo, 37.4133, -122.1162), -1) AS distanceInMiles,
         COLLECT_LIST(profileId) AS riders,
         COUNT(*) AS count
  FROM currentLocation
  GROUP BY ROUND(GEO_DISTANCE(la, lo, 37.4133, -122.1162), -1);

# Query a stream
-- Mountain View lat, long: 37.4133, -122.1162
SELECT * FROM riderLocations
  WHERE GEO_DISTANCE(latitude, longitude, 37.4133, -122.1162) <= 5 EMIT CHANGES;

# Login to another client session
docker exec -it ksqldb-cli ksql http://ksqldb-server:8088

# Poputate the stream
INSERT INTO riderLocations (profileId, latitude, longitude) VALUES ('c2309eec', 37.7877, -122.4205);
INSERT INTO riderLocations (profileId, latitude, longitude) VALUES ('18f4ea86', 37.3903, -122.0643);
INSERT INTO riderLocations (profileId, latitude, longitude) VALUES ('4ab5cbad', 37.3952, -122.0813);
INSERT INTO riderLocations (profileId, latitude, longitude) VALUES ('8b6eae59', 37.3944, -122.0813);
INSERT INTO riderLocations (profileId, latitude, longitude) VALUES ('4a7c7b41', 37.4049, -122.0822);
INSERT INTO riderLocations (profileId, latitude, longitude) VALUES ('4ddad000', 37.7857, -122.4011);

# Run another qiery against materialized view
SET 'ksql.query.pull.table.scan.enabled'='true';
SELECT * from ridersNearMountainView WHERE distanceInMiles <= 10;


# Run container for unit testing
docker build --rm -t ingestion-pipeline-spark_2 -f local_env/PySparkTestsDockerfile .
docker run -it -v "$PWD":/home --rm ingestion-pipeline-spark_2 /bin/bash
