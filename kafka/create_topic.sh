#!/bin/sh

if [ -z "$1" ]
then
    echo
    echo "--> Put topic name as argument to execute!"
    echo
    exit 1
fi
if [ -z "$2" ]
then
    echo
    echo "--> Put partitions number as argument to execute!"
    echo
    exit 1
fi
if [ -z "$3" ]
then
    echo
    echo "--> Put replication factor as argument to execute!"
    echo
    exit 1
fi

docker exec dhp_broker \
    bash -c "sh opt/bitnami/kafka/bin/kafka-topics.sh --bootstrap-server broker:9093 \
            --create \
            --topic $1 \
            --partitions $2 \
            --replication-factor $3"