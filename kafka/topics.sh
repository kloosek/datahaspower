#!/bin/sh

docker exec dhp_broker \
    bash -c "sh opt/bitnami/kafka/bin/kafka-topics.sh --bootstrap-server broker:9092 --list"