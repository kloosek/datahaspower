#!/bin/sh

if [ -z "$1" ]
then
    echo
    echo "--> Put topic name as argument to execute!"
    echo
    exit 1
fi

docker exec dhp_broker \
    bash -c "sh opt/bitnami/kafka/bin/kafka-topics.sh --describe --topic $1 --bootstrap-server broker:9093"