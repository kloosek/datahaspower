########################
#
# Simple kafka consumer
#
########################

import json

from kafka import KafkaConsumer, TopicPartition


TOPIC = "dhp_dummy_message_1"

KEY_1 = "saw"
KEY_2 = "engine"
KEY_3 = "pump"


def json_deserializer(msg):
    return json.loads(msg.decode("utf-8"))


consumer = KafkaConsumer(
    bootstrap_servers="localhost:9093",
    auto_offset_reset="earliest",
    value_deserializer=json_deserializer,
)


def read_messages(consumer, topic):
    consumer.subscribe([topic])
    for msg in consumer:
        print(msg.value)
    consumer.close()


if __name__ == "__main__":
    read_messages(consumer, TOPIC)
