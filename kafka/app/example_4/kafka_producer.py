##################################################################
#
# Simple kafka producer witk retry policy and confirmtion (acks)
#
##################################################################

import json

from kafka import KafkaProducer


PATH = "../../../data/source/kafka_input/"
INPUT_DATA_1 = PATH + "events_1.json"  # saw
INPUT_DATA_2 = PATH + "events_2.json"  # engine
INPUT_DATA_3 = PATH + "events_3.json"  # pump

TOPIC = "dhp_dummy_message_1"


def json_serializer(msg):
    return json.dumps(msg).encode("utf-8")


producer = KafkaProducer(
    bootstrap_servers="localhost:9093",
    value_serializer=json_serializer,
    retries=3,
    acks=1,  # 0 - don't wait, 1 - leader ack, all - all nodes ack
)


def send_messages(events_list, topic):
    for event in events_list:
        producer.send(topic, value=event)
    producer.flush()
    producer.close()


def read_input_data(path):
    with open(path, "r") as f:
        return [json.loads(line.strip()) for line in f.readlines()]


def main():
    data = read_input_data(INPUT_DATA_1)
    send_messages(data, TOPIC)


if __name__ == "__main__":
    main()
