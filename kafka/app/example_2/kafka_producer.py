###########################################
#
# Simple kafka producer with partition key
#
###########################################

import json

from kafka import KafkaProducer


PATH = "../../../data/source/kafka_input/"
INPUT_DATA_1 = PATH + "events_1.json"  # saw
INPUT_DATA_2 = PATH + "events_2.json"  # engine
INPUT_DATA_3 = PATH + "events_3.json"  # pump

TOPIC = "dhp_dummy_message_2"

KEY_1 = "saw"
KEY_2 = "engine"
KEY_3 = "pump"


def json_serializer(msg):
    return json.dumps(msg).encode("utf-8")


producer = KafkaProducer(
    bootstrap_servers="localhost:9093",
    key_serializer=str.encode,
    value_serializer=json_serializer,
)


def send_messages_by_key(events_list, topic, key):
    for event in events_list:
        producer.send(topic, key=key, value=event)
    producer.flush()
    producer.close()


def read_input_data(path):
    with open(path, "r") as f:
        return [json.loads(line.strip()) for line in f.readlines()]


def main():
    data = read_input_data(INPUT_DATA_1)
    send_messages_by_key(data, TOPIC, KEY_3)


if __name__ == "__main__":
    main()
