##############################################################
#
# Simple kafka consumer reads from given tiopic and partition
#
##############################################################

import json

from kafka import KafkaConsumer, TopicPartition


TOPIC = "dhp_dummy_message_1"

KEY_1 = "saw"
KEY_2 = "engine"
KEY_3 = "pump"

TOPICS_PARTITIONS = [
    (TOPIC, 0),
]


def json_deserializer(msg):
    return json.loads(msg.decode("utf-8"))


consumer = KafkaConsumer(
    bootstrap_servers="localhost:9093",
    auto_offset_reset="earliest",
    value_deserializer=json_deserializer,
)


def read_messages_from_partition(consumer, topics_partitions):
    subscribe_to = []
    for tp in topics_partitions:
        subscribe_to.append(TopicPartition(tp[0], tp[1]))
    consumer.assign(subscribe_to)
    for msg in consumer:
        print(msg.value)
    consumer.close()


if __name__ == "__main__":
    read_messages_from_partition(consumer, TOPICS_PARTITIONS)
