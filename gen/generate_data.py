import sys
import random
import csv
from datetime import datetime, timedelta

from hashlib import md5
from faker import Faker
from faker.providers import BaseProvider
from numpy import append


DATA_DIR = "../data/"
TOOLS_FILE_NAME = "source/tools.csv"
SAW_FILE_NAME = "source/saw/"
PUMP_FILE_NAME = "source/pump/"
ENGINE_FILE_NAME = "source/engine/"
HANDLER_FILE_NAME = "source/handler/"
CONVEYOR_FILE_NAME = "source/conveyor/"

TOOLS_CNT = 100
EVENTS_PER_FILE = 100
TIME_FAKED_DAYS = 40


class RandomChoiceProvider(BaseProvider):
    def tools(self):
        tool_types = ["saw", "pump", "engine", "handler", "conveyor"]
        return random.choice(tool_types)

    def lines(self):
        production_lines = ["1", "2", "3"]
        return random.choice(production_lines)

    def saw(self):
        return random.choice(get_ids_by_tool(DATA_DIR + TOOLS_FILE_NAME)["saw"])

    def pump(self):
        return random.choice(get_ids_by_tool(DATA_DIR + TOOLS_FILE_NAME)["pump"])

    def engine(self):
        return random.choice(get_ids_by_tool(DATA_DIR + TOOLS_FILE_NAME)["engine"])

    def handler(self):
        return random.choice(get_ids_by_tool(DATA_DIR + TOOLS_FILE_NAME)["handler"])

    def conveyor(self):
        return random.choice(get_ids_by_tool(DATA_DIR + TOOLS_FILE_NAME)["conveyor"])

    def get_event_time(self):
        return str(datetime.now())

    def get_event_time_faked(self):
        return str(datetime.now() - timedelta(days=TIME_FAKED_DAYS))


fake = Faker("pl_PL")
fake.add_provider(RandomChoiceProvider)


def to_csv(filename, line_list):
    with open(filename, "w") as f:
        for line in line_list:
            f.write(line)


def to_json(filename, line_list):
    with open(filename, "a") as f:
        for line in line_list:
            f.write(line + "\n")


def get_ids_by_tool(file_path):
    """Reads ids from CSV file."""
    saw_ids = []
    pump_ids = []
    engine_ids = []
    handler_ids = []
    conveyor_ids = []
    with open(file_path, "r") as f:
        header = True
        for line in csv.reader(f, delimiter=","):
            if header:  # Skip header line
                header = False
                continue
            if line[1] == "saw":
                saw_ids.append(line[0])
            elif line[1] == "pump":
                pump_ids.append(line[0])
            elif line[1] == "engine":
                engine_ids.append(line[0])
            elif line[1] == "handler":
                handler_ids.append(line[0])
            elif line[1] == "conveyor":
                conveyor_ids.append(line[0])

    return {
        "saw": saw_ids,
        "pump": pump_ids,
        "engine": engine_ids,
        "handler": handler_ids,
        "conveyor": conveyor_ids,
    }


def generate_tools(cnt=10):
    """Generate factory equipment."""
    tools = []

    for _ in range(cnt):
        tools = fake.csv(
            header=("Id", "Type", "ProdLineNo"),
            data_columns=("{{uuid4}}", "{{tools}}", "{{lines}}"),
            num_rows=100,
            include_row_ids=False,
        )

    return tools


def new_tools_list():
    to_csv(DATA_DIR + TOOLS_FILE_NAME, generate_tools(TOOLS_CNT))


def get_tool_ids():
    TOOLS_IDS_BY_TOOL = get_ids_by_tool(DATA_DIR + TOOLS_FILE_NAME)
    print(TOOLS_IDS_BY_TOOL)
    return TOOLS_IDS_BY_TOOL


def monitor_saw():
    signal = []
    signal.append(fake.json(
        data_columns=[
            ("Id", "saw"),
            ("Speed", "pyint", {"min_value": 1000, "max_value": 1200}),
            ("Temperature", "pyint", {"min_value": 150, "max_value": 200}),
            ("EventTime", "get_event_time"),
            # ("EventTime", "get_event_time_faked"),
        ],
        num_rows=1,
    ))

    return signal


def monitor_pump():
    signal = []
    signal.append(fake.json(
        data_columns=[
            ("Id", "pump"),
            ("Presure", "pyint", {"min_value": 115, "max_value": 120}),
            ("Temperature", "pyint", {"min_value": 50, "max_value": 70}),
            ("EventTime", "get_event_time"),
        ],
        num_rows=1,
    ))

    return signal


def monitor_engine():
    signal = []
    signal.append(fake.json(
        data_columns=[
            ("Id", "engine"),
            ("Speed", "pyint", {"min_value": 1000, "max_value": 1050}),
            ("Temperature", "pyint", {"min_value": 80, "max_value": 90}),
            ("EventTime", "get_event_time"),
        ],
        num_rows=1,
    ))

    return signal


def monitor_handler():
    signal = []
    signal.append(fake.json(
        data_columns=[
            ("Id", "handler"),
            ("Speed", "pyint", {"min_value": 5, "max_value": 10}),
            ("Power", "pyint", {"min_value": 150, "max_value": 200}),
            ("EventTime", "get_event_time"),
        ],
        num_rows=1,
    ))

    return signal


def monitor_conveyor():
    signal = []
    signal.append(fake.json(
        data_columns=[
            ("Id", "conveyor"),
            ("Speed", "pyint", {"min_value": 3, "max_value": 7}),
            ("Temperature", "pyint", {"min_value": 30, "max_value": 45}),
            ("EventTime", "get_event_time"),
        ],
        num_rows=1,
    ))

    return signal


def generate_tool_signals(cnt=5):
    """Generate signls of factory tools."""
    import time
    for _ in range(cnt):
        tmp_name = str(md5(str(datetime.now()).encode("utf-8")).hexdigest()) + ".json"
        for _ in range(EVENTS_PER_FILE):
            to_json(DATA_DIR + SAW_FILE_NAME + tmp_name, monitor_saw())
            to_json(DATA_DIR + PUMP_FILE_NAME + tmp_name, monitor_pump())
            to_json(DATA_DIR + ENGINE_FILE_NAME + tmp_name, monitor_engine())
            to_json(DATA_DIR + HANDLER_FILE_NAME + tmp_name, monitor_handler())
            to_json(DATA_DIR + CONVEYOR_FILE_NAME + tmp_name, monitor_conveyor())
            time.sleep(1.5)

if __name__ == "__main__":
    # How to run:
    # - python generate_data.py init -> to create tools.csv file
    # - python generate_data.py -> to generate events

    if len(sys.argv) > 1 and sys.argv[1] == "init":
        print("initiating tools list...")
        new_tools_list()
    else:
        print("Monitoring tools...")
        generate_tool_signals()
