#!/bin/bash

if [ -z "$1" ]
then
    echo
    echo "--> Put SQL script as argument to execute!"
    echo
    exit 1
fi

APP=app/$1
docker exec -i dhp_mysql bash -c "mysql --user=datahp --password=datahp --database=data_has_power" < $APP