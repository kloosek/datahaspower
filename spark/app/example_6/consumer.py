################################################
#
# Example 1
# Streaming from files and sink to console.
#
################################################

from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from pyspark.sql.types import (
    StructField,
    StructType,
    StringType,
    IntegerType,
    TimestampType,
)

CHECK_POINT_LOC = "/tmp/checkpointLocation/"

DATA_SOURCE_DIR = "/tmp/source/"
DATA_SINK_DIR = "/tmp/sink/"
SAW_FILE_NAME = "saw"
SAW_SINK = "saw_json"


def main():
    spark = SparkSession.builder.appName("dhp_example_1").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")

    events_saw = (
        spark.readStream
        .format("kafka")
        .option("kafka.bootstrap.servers", "localhost:9093")
        .option("topic", "dhp_dummy_message_4")
        .load()
    )

    events_saw.printSchema()

    events_saw = (
        events_saw.withColumn("EventTime", F.to_timestamp(F.col("EventTime")))
        .withWatermark("EventTime", "5 days")
        .groupBy("Id", F.window("EventTime", "2 minutes", "1 minutes"))
        .agg(
            F.avg("Speed").alias("AvgSpeed"),
            F.avg("Temperature").alias("AvgTemperature"),
        )
    )

    sink_saw = (
        events_saw.writeStream.format("console")
        .outputMode("complete")
        .start()
        .awaitTermination()
    )


if __name__ == "__main__":
    main()

# TODO Add sink for other sources
