from pyspark.sql import SparkSession
from pyspark.sql import functions as F


DATA_SOURCE_DIR = "/tmp/source/"
SAW_FILE_NAME = "saw"
OUTPUT = "/tmp/sink/xxx/"


def aggregate(df):
    df = df.filter(F.col("Temperature") < 170).groupBy("Id").count()
    return df.select("Id", F.col("count").alias("ItemsCount"))


def save_data(df):
    df.write.mode("overwrite").json(OUTPUT)


def main():
    spark = SparkSession.builder.appName("dhp_example_5").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")

    df = spark.read.option("inferSchema", "true").json(DATA_SOURCE_DIR + SAW_FILE_NAME)

    df = aggregate(df)
    save_data(df)

    spark.stop()


if __name__ == "__main__":
    main()
