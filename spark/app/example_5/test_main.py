import pytest
import pandas as pd

from main import aggregate

from pyspark.sql.types import (
    StructField,
    StructType,
    StringType,
    IntegerType,
)


def assert_dataframes_equal(data_1, data_2, columns):
    data_1 = data_1.toPandas().sort_values(columns).reset_index(drop=True)
    data_2 = data_2.toPandas().sort_values(columns).reset_index(drop=True)
    pd.testing.assert_frame_equal(data_1, data_2, check_like=True, check_dtype=False)


schema = StructType(
    [
        StructField("Id", StringType(), True),
        StructField("Speed", IntegerType(), True),
        StructField("Temperature", IntegerType(), True),
        StructField("EventTime", StringType(), True),
    ]
)

schema_expected = StructType(
    [
        StructField("Id", StringType(), True),
        StructField("ItemsCount", IntegerType(), True),
    ]
)


@pytest.mark.parametrize(
    ("data", "expected"),
    [
        pytest.param(
            [
                {
                    "Id": "1",
                    "Speed": 125,
                    "Temperature": 200,
                    "EventTime": "2022-04-08 01:17:00.951629",
                },
                {
                    "Id": "2",
                    "Speed": 100,
                    "Temperature": 120,
                    "EventTime": "2022-04-08 01:17:00.951629",
                },
                {
                    "Id": "2",
                    "Speed": 25,
                    "Temperature": 150,
                    "EventTime": "2022-04-08 01:17:00.951629",
                },
                {
                    "Id": "4",
                    "Speed": 95,
                    "Temperature": 150,
                    "EventTime": "2022-04-08 01:17:00.951629",
                },
                {
                    "Id": "7",
                    "Speed": 300,
                    "Temperature": 120,
                    "EventTime": "2022-04-08 01:17:00.951629",
                },
                {
                    "Id": "6",
                    "Speed": 300,
                    "Temperature": 200,
                    "EventTime": "2022-04-08 01:17:00.951629",
                },
                {
                    "Id": "7",
                    "Speed": 300,
                    "Temperature": 100,
                    "EventTime": "2022-04-08 01:17:00.951629",
                },
            ],
            [
                {"Id": "2", "ItemsCount": 2},
                {"Id": "4", "ItemsCount": 1},
                {"Id": "7", "ItemsCount": 2},
            ],
            id="case 1",
        ),
    ],
)
def test_aggregare(data, expected, spark_session):
    df_in = spark_session.createDataFrame(data, schema)
    df_expected = spark_session.createDataFrame(expected, schema_expected)
    resp_df = aggregate(df_in)
    assert_dataframes_equal(resp_df, df_expected, ["Id", "ItemsCount"])
