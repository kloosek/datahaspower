################################################
#
# Example 4
# Join stream with other stream and sink to MySQL.
#
################################################

import os
from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from pyspark.sql.types import (
    StructField,
    StructType,
    StringType,
    IntegerType,
    TimestampType,
)

CHECK_POINT_LOC = "/tmp/checkpointLocation/"

DATA_SOURCE_DIR = "/tmp/source/"
DATA_SINK_DIR = "/tmp/sink/"
SAW_FILE_NAME = "saw/*"
SAW_SINK = "sink_saw_json"
PUMP_FILE_NAME = "pump/*"
TOOLS_FILE_NAME = "tools.csv"

db_host = os.environ["MYSQL_HOST"]
db_port = os.environ["MYSQL_PORT"]
db_user = os.environ["MYSQL_USER"]
db_pass = os.environ["MYSQL_PASSWORD"]
db_db = os.environ["MYSQL_DATABASE"]


def main():
    spark = (
        SparkSession.builder.appName("dhp_example_4")
        .config("spark.sql.streaming.statefulOperator.checkCorrectness.enabled", False)
        .getOrCreate()
    )
    spark.sparkContext.setLogLevel("ERROR")

    events_saw_schema = StructType(
        [
            StructField("Id", StringType(), False),
            StructField("Speed", IntegerType(), False),
            StructField("Temperature", IntegerType(), False),
            StructField("EventTime", TimestampType(), False),
        ]
    )

    events_pump_schema = StructType(
        [
            StructField("Id", StringType(), False),
            StructField("Presure", IntegerType(), False),
            StructField("Temperature", IntegerType(), False),
            StructField("EventTime", TimestampType(), False),
        ]
    )

    tools_df = spark.read.option("header", "true").csv(
        DATA_SOURCE_DIR + TOOLS_FILE_NAME
    )
    saw_line_1_df = tools_df.filter(
        (F.col("ProdLineNo") == "1") & (F.col("Type") == "saw")
    )
    pump_line_1_df = tools_df.filter(
        (F.col("ProdLineNo") == "1") & (F.col("Type") == "pump")
    )

    # Prepare information about saws
    events_saw = (
        (
            spark.readStream.schema(events_saw_schema)
            .format("json")
            .option("latestFirst", "true")
            .option("maxFilesPerTrigger", "5")
            .load(DATA_SOURCE_DIR + SAW_FILE_NAME)
        )
        .join(saw_line_1_df, "Id", "left_outer")
        .select(
            F.col("Id").alias("SawId"),
            F.col("Type").alias("SawType"),
            F.col("ProdLineNo").alias("SawProdLineNo"),
            F.to_timestamp(F.col("EventTime")).alias("SawEventTime"),
            F.col("Speed").alias("SawSpeed"),
            F.col("Temperature").alias("SawTemperature"),
        )
    )

    # Prepare information about pumps
    events_pump = (
        (
            spark.readStream.schema(events_pump_schema)
            .format("json")
            .option("latestFirst", "true")
            .option("maxFilesPerTrigger", "5")
            .load(DATA_SOURCE_DIR + PUMP_FILE_NAME)
        )
        .join(pump_line_1_df, "Id", "left_outer")
        .select(
            F.col("Id").alias("PumpId"),
            F.col("Type").alias("PumpType"),
            F.col("ProdLineNo").alias("PumpProdLineNo"),
            F.to_timestamp(F.col("EventTime")).alias("PumpEventTime"),
            F.col("Presure").alias("PumpPresure"),
            F.col("Temperature").alias("PumpTemperature"),
        )
    )

    # Join streams
    events_saw_watermark = events_saw.withWatermark("SawEventTime", "10 days")
    events_pump_watermark = events_pump.withWatermark("PumpEventTime", "10 days")
    tools_joined_df = events_saw_watermark.join(
        events_pump_watermark,
        F.expr(
            """
            SawProdLineNo = PumpProdLineNo AND
            SawEventTime >= PumpEventTime AND
            SawEventTime < PumpEventTime + interval 5 second
            """
        ),
        "leftOuter",
    )

    print("-" * 100)
    tools_joined_df.printSchema()
    print("-" * 100)

    events = (
        tools_joined_df.withWatermark("SawEventTime", "10 days")
        .groupBy(
            F.window("SawEventTime", "5 seconds"),
            "SawId",
            "PumpId",
            "SawType",
            "Pumptype",
            "SawProdLineNo",
        )
        .agg(
            F.round(F.avg("SawSpeed")).alias("SawAvgSpeed"),
            F.round(F.avg("PumpPresure")).alias("PumpAvgPresure"),
            F.round(F.avg("SawTemperature")).alias("SawAvgTemperature"),
            F.round(F.avg("PumpTemperature")).alias("PumpAvgTemperature"),
        )
    )

    events_stat = events.withColumn(
        "SawStatus",
        F.when(
            F.col("SawAvgSpeed").between(1100, 1150)
            & F.col("SawAvgTemperature").between(160, 180),
            "OK",
        ).otherwise("Warning"),
    ).withColumn(
        "PumpStatus",
        F.when(
            F.col("PumpAvgPresure").between(1100, 1150)
            & F.col("PumpAvgTemperature").between(160, 180),
            "OK",
        ).otherwise("Warning"),
    )

    # Prepare output
    tools_joined_df = events_stat.select(
        F.col("window.start").alias("EventTime"),
        "SawId",
        "SawType",
        "SawStatus",
        "PumpId",
        "PumpType",
        "PumpStatus",
        F.col("SawProdLineNo").alias("ProdLineNo"),
    )

    def sink_batch(df, epoch_id):
        db_properties = {
            "user": f"{db_user}",
            "password": f"{db_pass}",
        }

        df.write.mode("append").jdbc(
            url=f"jdbc:mysql://{db_host}:{db_port}/{db_db}",
            table="agg_line_information",
            properties=db_properties,
        )

    sink_json = (
        tools_joined_df.writeStream.trigger(once=True)
        .outputMode("append")
        .option("checkpointLocation", CHECK_POINT_LOC)
        .foreachBatch(sink_batch)
        .start()
        .awaitTermination()
    )


if __name__ == "__main__":
    main()

# TODO Add other joins and sinks
