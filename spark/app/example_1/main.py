################################################
#
# Example 1
# Streaming from files and sink to console.
#
################################################

from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from pyspark.sql.types import (
    StructField,
    StructType,
    StringType,
    IntegerType,
    TimestampType,
)

CHECK_POINT_LOC = "/tmp/checkpointLocation/"

DATA_SOURCE_DIR = "/tmp/source/"
DATA_SINK_DIR = "/tmp/sink/"
SAW_FILE_NAME = "saw"
SAW_SINK = "saw_json"


def main():
    spark = SparkSession.builder.appName("dhp_example_1").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")

    # df = spark.read.option("inferSchema", "true").json(DATA_SOURCE_DIR + SAW_FILE_NAME)
    # df.printSchema()
    # df.show()

    events_saw_schema = StructType(
        [
            StructField("Id", StringType(), False),
            StructField("Speed", IntegerType(), False),
            StructField("Temperature", IntegerType(), False),
            StructField("EventTime", TimestampType(), False),
        ]
    )

    events_saw = (
        spark.readStream.schema(events_saw_schema)
        .format("json")
        # .option("latestFirst", "true")
        # .option("maxFilesPerTrigger", "5")
        .option("maxFileAge", "1d")
        .load(DATA_SOURCE_DIR + SAW_FILE_NAME)
    )

    events_saw.printSchema()

    events_saw = (
        events_saw.withColumn("EventTime", F.to_timestamp(F.col("EventTime")))
        .withWatermark("EventTime", "5 days")
        .groupBy("Id", F.window("EventTime", "2 minutes", "1 minutes"))
        .agg(
            F.avg("Speed").alias("AvgSpeed"),
            F.avg("Temperature").alias("AvgTemperature"),
        )
    )

    sink_saw = (
        events_saw.writeStream.format("console")
        .outputMode("complete")
        .start()
        .awaitTermination()
    )


if __name__ == "__main__":
    main()

# TODO Add sink for other sources
