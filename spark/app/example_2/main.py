################################################
#
# Example 2
# Streaming from files and sink to MySQL.
#
################################################

import os
from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from pyspark.sql.types import (
    StructField,
    StructType,
    StringType,
    IntegerType,
    TimestampType,
)

CHECK_POINT_LOC = "/tmp/checkpointLocation/"

DATA_SOURCE_DIR = "/tmp/source/"
DATA_SINK_DIR = "/tmp/sink/"
SAW_FILE_NAME = "saw"
SAW_SINK = "sink_saw_json"

db_host = os.environ["MYSQL_HOST"]
db_port = os.environ["MYSQL_PORT"]
db_user = os.environ["MYSQL_USER"]
db_pass = os.environ["MYSQL_PASSWORD"]
db_db = os.environ["MYSQL_DATABASE"]


def main():
    spark = SparkSession.builder.appName("dhp_example_2").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")

    events_saw_schema = StructType(
        [
            StructField("Id", StringType(), False),
            StructField("Speed", IntegerType(), False),
            StructField("Temperature", IntegerType(), False),
            StructField("EventTime", TimestampType(), False),
        ]
    )

    events_saw = (
        spark.readStream.schema(events_saw_schema)
        .format("json")
        .option("latestFirst", "true")
        .option("maxFilesPerTrigger", "5")
        .load(DATA_SOURCE_DIR + SAW_FILE_NAME)
    )

    events_saw = (
        events_saw.withColumn("EventTime", F.to_timestamp(F.col("EventTime")))
        .withWatermark("EventTime", "10 seconds")
        .groupBy(F.window("EventTime", "5 seconds"), "Id")
        .agg(
            F.round(F.avg("Speed")).alias("AvgSpeed"),
            F.round(F.avg("Temperature")).alias("AvgTemperature"),
        )
    )

    def sink_batch(df, epoch_id):
        # Use it when persisting to multiple sinks.
        # Avoid separate processing for every sink.
        # db_properties.persist()
        db_properties = {
            "user": f"{db_user}",
            "password": f"{db_pass}",
        }

        df = df.select(
            "Id",
            F.col("window.start").alias("time_window"),
            "AvgSpeed",
            "AvgTemperature",
        )
        df.write.mode("append").jdbc(
            url=f"jdbc:mysql://{db_host}:{db_port}/{db_db}",
            table="agg_saw",
            properties=db_properties,
        )
        # Clear persisted data.
        # db_properties.unpersist()

    sink_json = (
        events_saw.writeStream.outputMode("append")
        .option("checkpointLocation", CHECK_POINT_LOC)
        .foreachBatch(sink_batch)
        .start()
        .awaitTermination()
    )


if __name__ == "__main__":
    main()

# TODO Add other sinks
