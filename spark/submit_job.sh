#!/bin/bash

if [ -z "$1" ]
then
    echo
    echo "--> Put spark/pyspark script as argument to execute!"
    echo
    exit 1
fi

APP=/home/jovyan/app/$1
# docker exec -it dhp_spark bash -c "/usr/spark-2.3.0/bin/spark-submit --driver-memory 1G --executor-memory 1G $APP"
docker exec -it dhp_spark bash -c "/usr/local/spark/bin/spark-submit --driver-memory 1G --executor-memory 1G $APP"
# --files $SRC_PATH/functions.py,$SRC_PATH/schema_catalog.py
